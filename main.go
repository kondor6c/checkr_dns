package main

import (
	"flag"
	"gitlab.com/kondor6c/checkr_dns.git/pkg/resolv"
	"log"
	"net/http"
)

func main() {
	//common := []string{"www", "mail", "webmail", "utils", "test", "forum", "wiki", "exchange", "mongo", "nextcloud", "mysql", "imap", "smtp", "dev", "owa", "reports", "ns1", "ns2"}
	//guess := []string{"shop", "app"}
	//commonPowerTools := []string{"ombi", "plex", "prometheus", "deluge", "radarr", "sonarr", ""}
	var qRecord string
	var qType string
	var qServer string
	var web string
	flag.StringVar(&qRecord, "r", "", "first positional, the record for the query")
	flag.StringVar(&qType, "t", "A", "second positional, the desired DNS type")
	flag.StringVar(&qServer, "a", "9.9.9.9", "third positional, query this server for the record")
	flag.StringVar(&web, "h", "http", "http server")
	flag.Parse()
	if len(web) >= 1 {
		mux := http.NewServeMux()
		log.Println("listening for queries")
		mux.HandleFunc("/api/dns/", resolv.SimpleHandler)
		mux.HandleFunc("/api/walk/", resolv.WalkHandler)
		mux.HandleFunc("/api/record/", resolv.RecordHandler)
		http.ListenAndServe(":5001", mux)

	}
	result, err := resolv.SimpleRecord(qRecord, qType, "9.9.9.9")
	if err != nil {
		log.Println("hit error outer")
		log.Println(err)
	}
	log.Println("printing result")
	log.Println(result)
}
