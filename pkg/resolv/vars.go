package resolv

import (
	"net"
	"time"
)

type Question struct {
	QuestionName string   `json:"name" db:""`           // Following Google DoH json structure
	Type         string   `json:"type" db:""`           // type of dns record
	Server       string   `json:"server" db:""`         // the server being queried, could be "any" or not present
	CheckSec     bool     `json:"CD,omitempty" db:""`   // option: Checking Disabled
	Ipv6         bool     `json:"ipv6,omitempty" db:""` // whether to ask only AAAA and give back only AAAA (perhaps... might be an issue if the ask type is A?!? hmmm)
	Crawl        []string `json:"crawl,omitempty"`      // crawl, or maybe blindly search for any labels in the list. This should be a premium feature since it could be abused. Work will need to be done to integrate into the chase type....
	ChaseType    string   `json:"chase,omitempty"`
	/* custom option, continue searching for further records, if we found an NS that is of a different TLD (asked for tld example.org, got NS example.com), we will get A or AAAA records for those NS from the root servers. Similar with a CNAME, dev.example.com that points to elb.us-east-1.amazonaws.com, we will ask the other servers until we find an A record the target or MX or TXT  */
	ClientSubnet string `json:"edns_client_subnet,omitempty" db:""` // For the purposes of geo based responses, IF this is a timezone it will use that as the region
	Status       int    `json:"Status,omitempty" db:""`             // NOERROR - Standard DNS response code (32 bit integer)

}

type Response struct {
	Question      Question       `json:"name" db:""`
	Answer        []SingleAnswer `json:"Answer,omitempty" db:""`    // Following Google DoH json structure
	Chain         []SingleAnswer `json:"additional,omitempty"`      // Nameservers or records that lead to the final result, ie: mx IP, the MX records would be there, or NS records for an A record
	Elapsed       time.Duration  `json:"Elapsed,omitempty" db:""`   // Total time that the query took
	Responder     string         `json:"Responder,omitempty" db:""` // which NS server responded with the answer, ie 9.9.9.9
	AskedLocation string         `json:"Location,omitempty" db:""`  // For the purposes of geo based responses
	Secured       bool           `json:"DO,omitempty" db:""`        // response: DNSSEC secured response
	Status        int            `json:"Status,omitempty" db:""`    // NOERROR - Standard DNS response code (32 bit integer)
}

// follows google DoH JSON
type SingleAnswer struct {
	Type      string   `json:"Type,omitempty" db:""`
	Name      string   `json:"Name,omitempty" db:""`
	TTL       uint32   `json:"TTL,omitempty" db:""`
	IP        []net.IP `json:"IP,omitempty"`
	Data      []string `json:"Data,omitempty" db:""`
	Priority  uint16   `json:"priority,omitempty" db:""`
	Weight    uint16   `json:"weight,omitempty" db:""`
	Port      uint16   `json:"port,omitempty" db:""`
	Key       uint16   `json:"key,omitempty"`
	Tag       string   `json:"tag"`
	Signature string   `json:"signature,omitempty" db:""` // for the purposes of DNSSEC
	Algorithm string   `json:"algorithm,omitempty" db:""`
	Hash      []byte   `json:"hash,omitempty" db:""`
	HashType  string   `json:"hash,omitempty" db:""`
	Authority string
	Meta      Meta `json:"meta"`
}
type dnssec struct {
	Status     string // verified: we checked it, invalid: we found signed records and denied it, not found: rrsig records were not seen, unable: dnskey not found
	Expiration time.Time
	Creation   time.Time
}

type Meta struct {
	Started      time.Time     `json:"started,omitempty"`
	Completed    time.Time     `json:"Completed,omitempty"`
	Duration     time.Duration `json:"Duration,omitempty"`
	Valid        bool
	Relationship string
}
