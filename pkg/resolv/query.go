package resolv

import (
	"errors"
	"log"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/miekg/dns"
)

/*
https://2600index.info/Links/17/3/www.dns.net/dnsrd/tools.html
examples:
https://www.digwebinterface.com/
https://www.kitterman.com/spf/validate.html
https://forms.fbi.gov/check-to-see-if-your-computer-is-using-rogue-DNS
https://manytools.org/network/query-dns-records-online/
https://www.ipvoid.com/
http://en.dnstools.ch/port-scan.html
https://pentest-tools.com/information-gathering/find-subdomains-of-domain
https://dnslookup.online/txt.html
https://ipleak.net/
https://dns-lookup.whoisxmlapi.com/api
https://zonecheck.org/afk.php
*/

var DebugSet = false
var RootNS = []string{
	"a.root-servers.net.",
	"b.root-servers.net.",
	"c.root-servers.net.",
	"d.root-servers.net.",
	"e.root-servers.net.",
	"f.root-servers.net.",
	"g.root-servers.net.",
	"h.root-servers.net.",
	"i.root-servers.net.",
	"j.root-servers.net.",
	"k.root-servers.net.",
	"l.root-servers.net.",
	"m.root-servers.net.",
}

//func getIP(record string) net.IP {
//}
//func authorityName(record string) string {
//}
/*
func walk(record string) string {
	var next string
	if dns.IsFqdn(record) {
		next = strings.Join(strings.Split(record, ".")[:2], ".")
	}
	return next
}
*/
// GetOwner obtain the Authoritative server for the domain
func getOwner(record string) ([]SingleAnswer, error) {
	var timeout int = 30
	var numTimes int = 2
	var protoPort string = "53"
	var rootNS = []string{
		"a.root-servers.net.",
		"b.root-servers.net.",
		"c.root-servers.net.",
		"d.root-servers.net.",
		"e.root-servers.net.",
		"f.root-servers.net.",
		"g.root-servers.net.",
		"h.root-servers.net.",
		"i.root-servers.net.",
		"j.root-servers.net.",
		"k.root-servers.net.",
		"l.root-servers.net.",
		"m.root-servers.net.",
	}
	chainSlice := make([]SingleAnswer, 8)
	c := new(dns.Client)
	m := new(dns.Msg)

	domainSplit := dns.SplitDomainName(record)
	m.RecursionDesired = false
	var domainFull string
	var newResolvers []string
	var resolv []string
	var erro error
	config := &dns.ClientConfig{}

	for num := range domainSplit {
		reversed := len(domainSplit) - 1 - num
		dom := domainSplit[reversed]
		domainFull = dom + "." + domainFull
		if len(newResolvers) > 0 {
			log.Println("asking name servers ", newResolvers[0], "for")
			resolv = newResolvers
			newResolvers = []string{} // clear it out for good measure
		} else {
			log.Println("asking root name servers for")
			resolv = rootNS
		}
		config = &dns.ClientConfig{
			Servers:  resolv,
			Search:   []string{},
			Port:     protoPort,
			Ndots:    0,
			Timeout:  timeout,
			Attempts: numTimes,
		}
		m.SetQuestion(domainFull, dns.StringToType["NS"])
		r, _, err := c.Exchange(m, config.Servers[0]+":53")
		if err == nil && r != nil {
			rrResponse := &SingleAnswer{}
			log.Println("Response: ", r.Response)
			log.Println("Authroitative: ", r.Authoritative)
			log.Println("question: ", m.Question)
			log.Println("code: ", r.Opcode)
			if r.Authoritative == false {
				for _, n := range r.Ns { //Ns, is authority section
					log.Println("found Authority section")
					rrResponse.getTextDNSType(n)
					for _, e := range r.Extra { //Extra is "additional", e will be the full line of the extra
						log.Println("extra: ", e)
						rrResponse.getTextDNSType(e)
						//rrResponse.updateAddr(strings.Split(e.String(), " "))
						switch t := e.(type) {
						case *dns.AAAA:
							log.Println("t, item of extra is name: ", t.Header().Name)
							log.Println("[AAAA] NS to AAAA in additional", string(t.AAAA))
							newResolvers = append(newResolvers, t.AAAA.String())
						case *dns.A:
							log.Println("t, item of extra is name: ", t.Header().Name)
							log.Println("[A] NS to A in additional", string(t.A))
							newResolvers = append(newResolvers, t.A.String())
						}
					}
					if len(newResolvers) < 1 {
						log.Println("hit no resolvers")
						newResolvers = append(newResolvers, n.(*dns.NS).Ns)
						log.Println("added: ")
						log.Println(newResolvers[0])
					}
				}
			} else {
				for _, a := range r.Answer {
					for _, e := range r.Extra {
						log.Println("found Additional in the Answer")
						log.Println(e)
						rrResponse.getTextDNSType(e)
						//rrResponse.updateAddr(strings.Split(e.String(), " "))
					}
					log.Println("answer is: ")
					log.Println(a)
					rrResponse.getTextDNSType(a)
					if DebugSet == true {
						log.Println("found resource record query")
					}
					chainSlice = append(chainSlice, *rrResponse)
				}
				erro = nil
			}
		} else if err != nil {
			if DebugSet == true {
				log.Printf("hit error at SOA, and Rcode was: %d", r.Rcode)
			}
			log.Println("error at lookup", err)
			erro = err

		} else if r.Rcode != dns.RcodeSuccess {
			var rcode string
			if r.Rcode > 0 {
				rcode = strconv.Itoa(r.Rcode)
			}
			erro = errors.New("dns lookup failure when trying to nonrecursively resolve" + rcode)
		}
	}
	return chainSlice, erro
}
func askNoRecurse(record string, dnsType string) ([]SingleAnswer, error) {
	chain, err := getOwner(record)
	return chain, err
}

/* //This will not work! but it stands as a prototype as to what it might look like
func checkSign(s SingleAnswer) {
	resRecSig, rrserr := simpleRecord(s.Name, "RRSIG", s.Authority)
	if rrserr := nil {
		log.Println("ERROR could not find or obtain RRSIG for the record")
	}
	authKey, keyErr := simpleRecord(s.Authority, s.Name)
	resRecSig.Verify(authKey, s.toRRType(s.Type) )
}
*/// SimpleQuery acts like nslookup, so that other packages can use this, largely wrap SetQuestion and Exchange
func SimpleRecord(rec string, dnsType string, server string) (SingleAnswer,error) {
	return SimpleAnswer(Question{QuestionName: rec, Type: dnsType, Server: server})
}

// SimpleQuery acts like nslookup, so that other packages can use this, largely wrap SetQuestion and Exchange
func SimpleAnswer(q Question) (SingleAnswer,error) {
	var timeout int = 30
	var numTimes int = 2
	var protoPort string = "53"
	var nameservers = []string{}
	rrResponse := &SingleAnswer{}
	var rerr error
	c := new(dns.Client)
	m := new(dns.Msg)
	if q.Server == "any" {
		nameservers = []string{"9.9.9.9"}
		m.RecursionDesired = true
	} else if q.Server == "system" {
		nameservers = []string{"10.1.1.1"}
		m.RecursionDesired = true
		m.CheckingDisabled = false
	} else if q.Server == "root" {
		nameservers = RootNS
		m.RecursionDesired = false
	} else {
		nameservers = []string{q.Server}
		m.RecursionDesired = false
	}
	config := &dns.ClientConfig{
		Servers:  nameservers,
		Search:   []string{},
		Port:     protoPort,
		Ndots:    0,
		Timeout:  timeout,
		Attempts: numTimes,
	}
	m.SetQuestion(dns.Fqdn(q.QuestionName), dns.StringToType[strings.ToUpper(q.Type)])
	start := time.Now()
	r, _, err := c.Exchange(m, config.Servers[0]+":"+config.Port)
	rrResponse.Meta.Duration = time.Duration(time.Since(start))
	if err == nil && r != nil {
		for i, a := range r.Answer {
			rrResponse.getTextDNSType(a)
			log.Println("[dns] header ", a.Header().Name, " counter: ", i)
			if a.Header().Name == dns.Fqdn(q.QuestionName) {
				log.Printf("[dns] successful match")
			}
			log.Printf("[dns] %v\n", rrResponse)
			log.Printf("[dns] Name: %s", rrResponse.Name)
			for _, e := range r.Extra {
				log.Println("[dns] found Additional in the Answer, these should correlate to IP addresses")
				log.Println(e)
				//rrResponse.getTextDNSType(e)
				rrResponse.recordIP(e)
			}
		}
		rerr = nil
	} else if err != nil {
		if DebugSet == true {
			log.Printf("[dns] hit error at SOA, and Rcode was: %d", r.Rcode)
		}
		log.Println("[dns] error at lookup", err)
		rerr = err
	} else if r.Rcode != dns.RcodeSuccess {
		var rcode string
		if r.Rcode > 0 {
			rcode = strconv.Itoa(r.Rcode)
			rerr = errors.New(rcode)
		}
		rerr = err
	}

	return *rrResponse, rerr
}
func (s *SingleAnswer) putIP(search string, ip net.IP) {
	for _, i := range s.IP {
		if i.Equal(ip) {
			log.Println("[dns] no action needed IP already exists")
		} else {
			s.IP = append(s.IP, ip)
		}
	}
}
func (s *SingleAnswer) recordIP(r dns.RR) {
	recordName := strings.Split(r.String(), " ")[0]
	if recordName == s.Name {
		switch t := r.(type) {
		case *dns.AAAA:
			log.Println("[dns] Adding to answer: ", t.Header().Name)
			s.IP = append(s.IP, t.AAAA)
		case *dns.A:
			log.Println("[dns] Adding to answer: ", t.Header().Name)
			s.IP = append(s.IP, t.A)
		}
	} else {
		log.Println("no match of name from recording IP")
	}
}
func (a *Response) updateAddr(search string, ip net.IP) {
	for i, r := range a.Answer {
		if r.Name == search {
			a.Answer[i].IP = append(a.Answer[i].IP, ip)
		}
	}
}

func (r *SingleAnswer) getTextDNSType(singleRR dns.RR) {
	r.Name = singleRR.Header().Name
	r.TTL = singleRR.Header().Ttl
	log.Println("[internal] ingesting dns RR")
	switch t := singleRR.(type) {
	case *dns.AAAA:
		r.Type = "AAAA"
		r.IP = append(r.IP, t.AAAA)
	case *dns.A:
		r.Type = "A"
		r.IP = append(r.IP, t.A)
	case *dns.CAA:
		r.Type = "CAA"
		log.Println(t)
	case *dns.CNAME:
		r.Type = "CNAME"
		r.Data = []string{t.Target}
	case *dns.DNSKEY:
		r.Type = "DNSKEY"
	case *dns.DS: // delegation signer
		r.Type = "DS"
		//		r.Hash = t.Digest
		//		r.HashType = t.DigestType
		//		r.Algorithm = t.Algorithm
		//		r.Key = t.Key
		log.Println(t)
	case *dns.NS:
		r.Type = "NS"
		r.Data = []string{t.Ns}
	case *dns.NSEC:
		r.Type = "NSEC"
		r.Data = []string{t.NextDomain}
	case *dns.NSEC3:
		r.Type = "NSEC3"
		r.Data = []string{t.NextDomain}
	case *dns.NSEC3PARAM:
		r.Type = "NSEC3Param"
		r.Data = []string{t.Salt}
	case *dns.MX:
		r.Type = "MX"
		r.Weight = singleRR.(*dns.MX).Preference
		r.Data = []string{t.Mx}
	case *dns.OPENPGPKEY:
		r.Type = "OPENPGPKEY"
		log.Println(t)
	case *dns.SSHFP:
		r.Type = "SSHFP"
		r.Data = []string{t.FingerPrint}
		//r.Algorithm = singleRR.(*dns.SSHFP).Algorithm
		//r.Hash = singleRR.(*dns.SSHFP).Type
	case *dns.PTR:
		r.Type = "PTR"
		r.Data = []string{t.Ptr}
	case *dns.RRSIG:
		r.Type = "RRSIG"
	case *dns.SRV:
		r.Type = "SRV"
		r.Data = []string{t.Target}
		r.Priority = singleRR.(*dns.SRV).Priority
		r.Weight = singleRR.(*dns.SRV).Weight
		r.Port = singleRR.(*dns.SRV).Port

	case *dns.TLSA:
		r.Type = "TLSA"
		log.Println(t)
	case *dns.SOA:
		r.Type = "SOA"
		log.Println(t)
	case *dns.TXT:
		r.Type = "TXT"
		log.Println("[dns] found text")
		log.Println(t.Txt)
		r.Data = t.Txt
	}
}
