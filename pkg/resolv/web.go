package resolv

import (
	"encoding/json"
	"log"
	"net/http"
)

func RecordHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("obtained request for a no recursive ask of owner")
	q := new(Question)
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&q)

	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	results, qerr := SimpleRecord(q.QuestionName, q.Type, q.Server)
	if qerr != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	aR := &Response{Question: *q, Answer: []SingleAnswer{results}}
	jsonOut, jerr := json.Marshal(aR)
	if jerr != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	//w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Write(jsonOut)
	/*ans, aerr := json.Marshal(results)
	    if aerr != nil {
			http.Error(w, err.Error(), 400)
			return
		}*/

}
func WalkHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("obtained request for a no recursive ask of owner")
	q := new(Question)
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&q)

	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	results, qerr := askNoRecurse(q.QuestionName, q.Type)
	if qerr != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	aR := &Response{Question: *q, Answer: results}
	jsonOut, jerr := json.Marshal(aR)
	if jerr != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	//w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Write(jsonOut)
	/*ans, aerr := json.Marshal(results)
	    if aerr != nil {
			http.Error(w, err.Error(), 400)
			return
		}*/

}
func SimpleHandler(w http.ResponseWriter, r *http.Request) {
	log.Println("obtained request for Simple")
	q := new(Question)
	if r.Body == nil {
		http.Error(w, "Please send a request body", 400)
		return
	}
	err := json.NewDecoder(r.Body).Decode(&q)

	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	results, qerr := SimpleRecord(q.QuestionName, q.Type, q.Server)
	if qerr != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	//aR := &Response{Question: *q, Answer: results }
	jsonOut, jerr := json.Marshal(&Response{Question: *q, Answer: []SingleAnswer{results}})
	if jerr != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	w.Write([]byte(jsonOut))
	//w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	/*ans, aerr := json.Marshal(results)
	    if aerr != nil {
			http.Error(w, err.Error(), 400)
			return
		}*/

}
